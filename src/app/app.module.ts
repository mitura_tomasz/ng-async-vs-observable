import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ObservableMethodComponent } from './components/observable-method/observable-method.component';
import { AsyncMethodComponent } from './components/async-method/async-method.component';

@NgModule({
  declarations: [
    AppComponent,
    ObservableMethodComponent,
    AsyncMethodComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservableMethodComponent } from './observable-method.component';

describe('ObservableMethodComponent', () => {
  let component: ObservableMethodComponent;
  let fixture: ComponentFixture<ObservableMethodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObservableMethodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservableMethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

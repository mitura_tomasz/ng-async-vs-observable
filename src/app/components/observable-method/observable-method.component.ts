import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

@Component({
  selector: 'app-observable-method',
  templateUrl: './observable-method.component.html',
  styleUrls: ['./observable-method.component.scss']
})
export class ObservableMethodComponent implements OnInit {

  numbers$ = new BehaviorSubject<any>([1, 2, 3 ,4 ,5]);
  numbersSubscribtion: Subscription;
  evenNumbers: number[] = [];

  constructor() { }

  ngOnInit() {
    this.numbersSubscribtion =  this.numbers$.subscribe(responseNumbers => {
      
      this.evenNumbers = responseNumbers.filter(number => {
        if (number % 2 === 0) return number;
      })

    })
  }

  ngOnDestroy(): void {
    this.numbersSubscribtion.unsubscribe();
  }

}

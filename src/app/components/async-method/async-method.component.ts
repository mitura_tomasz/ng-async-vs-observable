import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-async-method',
  templateUrl: './async-method.component.html',
  styleUrls: ['./async-method.component.scss']
})
export class AsyncMethodComponent implements OnInit {

  numbers$ = new BehaviorSubject<any>([1, 2, 3 ,4 ,5]);


  constructor() { }

  ngOnInit() {
  }

}
